<?php
namespace App\Product;

use Predis\Client;

class Create
{
	public $client;
	/**
	 * @param Client $client
	 */
	public function __construct(Client $client)
	{
		$this->client = $client;
	}

	/**
	 * @param string $name
	 */
	public function create($name)
	{
		$this->client->set($name,null);
	}

	/**
	 * @param string $name
	 * @param int $punctuation
	 */
	public function rank($name, $punctuation)
	{

	}

}