<?php
namespace App\Product;

use Predis\Client;

class Punctuation
{
	public $client;
	public $name;
	/**
	 * @param Client $client
	 * @param string $name
	 */
	public function __construct(Client $client, $name)
	{
		$this->client = $client;
		$this->name = $name;
	}

	/**
	 * @return int|null
	 */
	public function get()
	{
		return $this->client->get($this->name)!=null?:null;

	}

	/**
	 * @param int $punctuation
	 */
	public function set($punctuation)
	{
		$this->client->set($this->name, $punctuation);

	}

}