# ![La Salle BES](http://jcarreras.es/images/lasalle.png)

# ![screenshot](.screenshot.png)

# Descripción
-----------------------

Programa una aplicación simple para puntuar productos

En este ejercicio te encontrarás con un esqueleto de código sin implementar que debes completar. Se trata de una
aplicación para dar de alta productos y puntuarlos, puediendo mostrar luego la media de puntuaciones recibida
por producto.


# Instalación
-----------------------

```
$ vagrant up
```


# Instrucciones
-----------------------

- Entra en la máquina por ssh con `vagrant ssh`
- Entra en el directorio src y ejecuta `composer update`
- Si entras en el directorio `src/examples` verás diferentes ejemplos de uso del cliente Predis. Ejecuta cada uno de ellos (ejemplo: `php 1_key_value.php`) y mira el código para comprender las diferentes maneras de operar con redis
- Abre el ficheor `src/tests/ExampleTest.php` para ver qué tienen que hacer las clases `src/App/Product/Create.php` y `src/App/Product/Punctuation.php`
- Des del directorio `~/src` puedes ejecutar el test mediante el comando `vendor/bin/phpunit`
- El test fallará hasta que no implmementes la funcionalidad que falta en las clases `src/App/Product/Create.php` y `src/App/Product/Punctuation.php`


# Desinstalación
-----------------------

```
$ vagrant destroy
```
